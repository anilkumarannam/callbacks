const fs = require('fs');
const path = require('path');

const getRandomName = (length) => {
  const nameCodes = [];
  for (let index = 0; index < length; index++) {
    nameCodes.push(Math.floor(Math.random() * (91 - 65) + 65));
  }
  return String.fromCharCode(...nameCodes).concat('.json').toLowerCase();
}
const deleteFile = (filePath) => {
  fs.unlink(filePath, (error) => {
    if (error) {
      console.error(error);
    } else {
      console.log(`File at ${filePath} has been deleted successfully..!!!`);
    }
  });
}
const createAndDeleteFile = (filePath, deleteFile) => {
  fs.appendFile(filePath, 'utf-8', (error, data) => {
    if (error) {
      console.error(error);
    } else {
      console.log(`File created at ${filePath}`);
      deleteFile(filePath);
    }
  });
}

const createAndDeleteRandomFiles = (directory, filesCount, nameLength, createAndDeleteFile) => {
  for (let index = 0; index < filesCount; index++) {
    const fileName = getRandomName(nameLength);
    const filePath = `${directory}/${fileName}`;
    if (fs.existsSync(filePath)) {
      console.log('file Exists');
    } else {
      createAndDeleteFile(filePath, deleteFile);
    }
  }
}


const createAndDeleteRadomFilesFromDirectory = (directory, filesCount, nameLength, createAndDeleteRandomFiles) => {

  fs.mkdir(path.join(__dirname, directory), (error, data) => {
    if (error) {
      if (error.errno === -17) {
        console.log(`Directory [${directory}] already exists`);
      }
    } else {
      console.log(`Directory [${directory}] created successfully..!!!`);
      createAndDeleteRandomFiles(directory, filesCount, nameLength, createAndDeleteFile);
    }
  });

}
const createDirectoryAndAddRandomFiles = (directory, filesCount, nameLength) => {
  createAndDeleteRadomFilesFromDirectory(directory, filesCount, nameLength, createAndDeleteRandomFiles);
};

module.exports = createDirectoryAndAddRandomFiles;