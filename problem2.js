const fs = require('fs');
const f = require('./functions');
const getRandomName = (length) => {
  const nameCodes = [];
  for (let index = 0; index < length; index++) {
    nameCodes.push(Math.floor(Math.random() * (91 - 65) + 65));
  }
  return String.fromCharCode(...nameCodes).concat('.txt').toLowerCase();
};

const deleteFile = (filePath) => {
  fs.unlink(filePath, (error) => {
    if (error) {
      console.error(error);
    } else {
      console.log(`File at ${filePath} has been deleted successfully..!!!`);
    }
  });
}

const readAndConvertToUpperCase = (fileName, createAFolderForTextFiles) => {
  try {
    fs.readFile(fileName, 'utf-8', function (error, data) {
      if (error) {
        console.error(error);
      } else {
        const convertedData = data.toUpperCase();
        createAFolderForTextFiles(convertedData, writeToANewFileInFolder);
      }
    });
  } catch (error) {
    console.error(error);
  }
};

const createAFolderForTextFiles = (convertedData, writeToANewFileInFolder) => {
  try {
    const folderName = 'textFiles';
    if (!fs.existsSync(folderName)) {
      fs.mkdir(folderName, (error) => {
        if (error) {
          throw error;
        } else {
          console.log(`${folderName} folder is created`);
          writeToANewFileInFolder(folderName, convertedData);
        }
      });
    } else {
      console.log(`${folderName} folder is is already exits`);
      writeToANewFileInFolder(folderName, convertedData);
    }
  } catch (error) {
    console.error(error);
  }
};

const writeToANewFileInFolder = (folderName, convertedData) => {
  const newFileName = getRandomName(5);
  try {
    fs.writeFile(`${folderName}/${newFileName}`, convertedData, 'utf-8', (error) => {
      if (error) {
        throw error;
      } else {
        console.log(`Data saved to ${folderName}/${newFileName}`);
        fs.appendFile('filenames.txt', `${newFileName}\n`, 'utf-8', (error) => {
          if (error) {
            throw error;
          } else {
            console.log(`File name ${newFileName} added to filenames.txt`);
            readTheNewFileAndConvertItLowerCase(folderName, newFileName, writeSentencesToNewFile);
          }
        });
      }
    });
  } catch (error) {
    console.error(error);
  }
};

const readTheNewFileAndConvertItLowerCase = (folderName, fileName, writeSentencesToNewFile) => {
  try {
    fs.readFile(`${folderName}/${fileName}`, 'utf-8', function (error, data) {
      if (error) {
        console.error(error);
      } else {
        const convertedData = data.toLowerCase();
        const sentences = convertedData.split(/\. |\n+/g);
        const sentencesData = sentences.reduce((previousValue, currentValue) => {
          if (currentValue !== '') {
            return previousValue + currentValue.charAt(0).toUpperCase() + currentValue.slice(1) + '.' + '\n';
          } else {
            return previousValue;
          }
        }, '');
        writeSentencesToNewFile(folderName, sentencesData, readSentencesAndSort);
      }
    });
  } catch (error) {
    console.error(error);
  }
};


const writeSentencesToNewFile = (folderName, sentencesData, readSentencesAndSort) => {
  const fileName = getRandomName(5);
  const filePath = `${folderName}/sentences${fileName.charAt(0).toUpperCase() + fileName.slice(1)}`;
  try {
    fs.writeFile(filePath, sentencesData, 'utf-8', (error) => {
      if (error) {
        throw error;
      } else {
        console.log(`Sentences are saved to ${filePath}`);
        fs.appendFile('filenames.txt', `${filePath.split('/')[1]}\n`, 'utf-8', (error) => {
          if (error) {
            throw error;
          } else {
            console.log(`File name ${filePath.split('/')[1]} added to filenames.txt`);
            readSentencesAndSort(folderName, fileName, writeSentencesToNewFile);
          }
        });
      }
    });
  } catch (error) {
    console.error(error);
  }
};

const readSentencesAndSort = (folderName, fileName, writeSentencesToNewFile) => {
  try {
    const filePath = `${folderName}/sentences${fileName.charAt(0).toUpperCase() + fileName.slice(1)}`;
    fs.readFile(filePath, 'utf-8', function (error, data) {
      if (error) {
        console.error(error);
      } else {
        const sentences = data.split('\n');
        const sortedSentences = sentences.sort().join('\n');
        console.log("Sentences are sorted")
        writeSentencesToNewFile(folderName, sortedSentences, readCreatedFileNamesAndDelete);
      }
    });
  } catch (error) {
    console.error(error);
  }
}

const readCreatedFileNamesAndDelete = (folderName) => {
  try {
    fs.readFile('filenames.txt', 'utf-8', function (error, data) {
      if (error) {
        console.error(error);
      } else {
        const fileNames = data.split('\n');
        fileNames.forEach((fileName) => {
          if (fileName !== '') {
            const filePath = `${folderName}/${fileName}`;
            deleteFile(filePath);
          }
        });
        fs.writeFile('filenames.txt','','utf-8',(error)=>{
          console.log("filenames.txt is empty");
        });
      }
    });
  } catch (error) {
    console.error(error);
  }
}

const manipulateFile = (fileName) => {
  readAndConvertToUpperCase(fileName, createAFolderForTextFiles);
};

module.exports = manipulateFile;